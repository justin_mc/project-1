﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CustomerList
/// </summary>
public class CustomerList
{
    private List<ListItem> _customerList;

	public CustomerList()
	{
		this._customerList = new List<ListItem>();
	}

    /// <summary>
    /// Returns the number of customers in the list.
    /// </summary>
    /// <returns>int</returns>
    public int Count {
        get { return this._customerList.Count(); }
    }

    public ListItem this[int index]
    {
        get { return this._customerList[index]; }
        set { this._customerList[index] = value; }
    }

    /// <summary>
    /// Gets the <see cref="ListItem"/> with the specified name.
    /// </summary>
    /// <value>
    /// The <see cref="ListItem"/>.
    /// </value>
    /// <param name="name">The name.</param>
    /// <returns></returns>
    public ListItem this[string name]
    {
        get
        {
            foreach (ListItem listitem in this._customerList)
                if (listitem.Customer.Name == name) return listitem;
            return null;
        }
    }

    /// <summary>
    /// Gets the list of customers from the current session state.
    /// </summary>
    /// <returns></returns>
    public static CustomerList GetList()
    {
        CustomerList list = (CustomerList)HttpContext.Current.Session["Name"];
        if (list == null)
        {
            HttpContext.Current.Session["Name"] = new CustomerList();
        }
        return (CustomerList) HttpContext.Current.Session["Name"];
    }

    /// <summary>
    /// Adds the item to the end of the list.
    /// </summary>
    /// <param name="newCustomer">The new customer.</param>
    public void AddItem(ListItem newCustomer)
    {
        this._customerList.Add(newCustomer);
    }

    /// <summary>
    /// Removes the customer at the specififed index.
    /// </summary>
    /// <param name="index">The index of the customer.</param>
    public void RemoveAt(int index)
    {
        this._customerList.RemoveAt(index);
    }

    /// <summary>
    /// Removes all elements from the customer list.
    /// </summary>
    public void Clear()
    {
        this._customerList.Clear();
    }
}
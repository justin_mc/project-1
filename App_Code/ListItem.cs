﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

/// <summary>
/// Summary description for ListItem
/// </summary>
public class ListItem
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ListItem"/> class.
    /// </summary>
    /// <param name="customer">The customer.</param>
	public ListItem(Customer customer)
	{
	    this.Customer = customer;
	}

    /// <summary>
    /// Gets or sets the customer.
    /// </summary>
    /// <value>
    /// The customer.
    /// </value>
    public Customer Customer { get; set; }

    public string Display()
    {
        string displayString = this.Customer.Name + ": " 
            + this.Customer.Phone + "; " + this.Customer.Email;
        return displayString;
    }
}
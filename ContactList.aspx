﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContactList.aspx.cs" Inherits="ContactList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Digital Upscale Manager for Ballgames</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
	<header>
	    <div id="logo">
	        <h1>Digital Upscale Manager for Ballgames</h1>
	        <h2>Managing Ballgames With Technology</h2>
        </div>
	</header>
        
        <section>
            <article>
                <p>
                    
                    <asp:ListBox ID="lstbxContacts" runat="server" OnSelectedIndexChanged="lstbxContacts_SelectedIndexChanged"></asp:ListBox>
                    
                </p>
                <p>
                    
                    <asp:Button ID="btnSelectAdditionalCustomers" runat="server" OnClick="btnSelectAdditionalCustomers_Click" Text="Select Additional Customers" />
                    <asp:Button ID="btnRemoveCustomer" runat="server" OnClick="btnRemoveCustomer_Click" Text="Remove Customer" />
                    <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" />
                    
                </p>
                <p>
                    
                    <asp:Label ID="lblMessages" runat="server" ForeColor="Red"></asp:Label>
                    
                </p>
            </article>
        </section>

        <!-- A copyright notice and disclaimer because why not -->
	<footer>©Copyright Justin McConnell 2015
        <br/>
        Disclaimer: Not responsible for any financial loss or bodily injury due to use of the information or code on this site.
	</footer>
    </form>
</body>
</html>

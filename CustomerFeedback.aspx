﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomerFeedback.aspx.cs" Inherits="CustomerFeedback" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Digital Upscale Manager for Ballgames</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
	<header>
	    <div id="logo">
	        <h1>Digital Upscale Manager for Ballgames</h1>
	        <h2>Managing Ballgames With Technology</h2>
        </div>
	</header>
    
    <!-- Middle section, all centered content and form stuff goes here -->
    <section>
        <article>
            <p>
                <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID:"></asp:Label>
                <asp:TextBox ID="txtCustomerID" runat="server"></asp:TextBox>
                <asp:Button ID="btnCustomerID" runat="server" Text="Submit" OnClick="btnCustomerID_Click" />
                <asp:RequiredFieldValidator ID="rfvCustomerID" runat="server" ControlToValidate="txtCustomerID" ErrorMessage="This field is required." ForeColor="Red"></asp:RequiredFieldValidator>
            </p>
            <p>
                <asp:ListBox ID="lstClosedFeedback" runat="server"></asp:ListBox>
            </p>
            <p>
                <asp:Label ID="lblServiceTime" runat="server" Text="Service Time:"></asp:Label>
                <asp:RadioButton ID="rbnServiceSatisfied" runat="server" GroupName="service" Text="Satisfied" Enabled="False" />
                <asp:RadioButton ID="rbnServiceNeither" runat="server" GroupName="service" Text="Neither" Enabled="False" />
                <asp:RadioButton ID="rbnServiceDissatisfied" runat="server" GroupName="service" Text="Dissatisfied" Enabled="False" />
                
                <br/>
                <asp:Label ID="lblTechEfficiency" runat="server" Text="Technical Efficiency:"></asp:Label>
                <asp:RadioButton ID="rbnEfficiencySatisfied" runat="server" GroupName="efficiency" Text="Satisfied" Enabled="False" />
                <asp:RadioButton ID="rbnEfficiencyNeither" runat="server" GroupName="efficiency" Text="Neither" Enabled="False" />
                <asp:RadioButton ID="rbnEfficiencyDissatisfied" runat="server" GroupName="efficiency" Text="Dissatisfied" Enabled="False" />
                <br/>
                <asp:Label ID="lblResolution" runat="server" Text="Problem Resolution:"></asp:Label>
                <asp:RadioButton ID="rbnResolutionSatisfied" runat="server" GroupName="resolution" Text="Satisfied" Enabled="False" />

                <asp:RadioButton ID="rbnResolutionNeither" runat="server" GroupName="resolution" Text="Neither" Enabled="False" />
                <asp:RadioButton ID="rbnResolutionDissatisfied" runat="server" GroupName="resolution" Text="Dissatisfied" Enabled="False" />

            </p>
            <p>
                <asp:Label ID="lblComments" runat="server" Text="Additional Comments:"></asp:Label>
                <br/>
                <asp:TextBox ID="TextBox1" runat="server" Enabled="False"></asp:TextBox>

            </p>
            <p>
                <asp:CheckBox ID="chbxContacted" runat="server" Enabled="False" Text="Yes, I wish to be contacted." />
                <asp:RadioButton ID="rbnEmail" runat="server" Enabled="False" GroupName="contact" Text="by Email" />
                <asp:RadioButton ID="rbnPhone" runat="server" Enabled="False" GroupName="contact" Text="by Phone" />

            </p>
            <p>
                <asp:Button ID="btnSubmitAll" runat="server" Enabled="False" OnClick="btnSubmitAll_Click" Text="Finish and Submit" />

            </p>
        </article>
    </section>
        </form>
        <!-- A copyright notice and disclaimer because why not -->
	<footer>©Copyright Justin McConnell 2015
        <br/>
        Disclaimer: Not responsible for any financial loss or bodily injury due to use of the information or code on this site.
	</footer>
</body>
</html>

﻿using System;

public partial class CustomerFeedback : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.SetFocus(this.txtCustomerID);
    }
    protected void btnCustomerID_Click(object sender, EventArgs e)
    {
        Response.Redirect("FeedbackComplete.aspx");
    }
    protected void btnSubmitAll_Click(object sender, EventArgs e)
    {
        Response.Redirect("FeedbackComplete.aspx");
    }
}
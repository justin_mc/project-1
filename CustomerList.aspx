﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomerList.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Digital Upscale Manager for Ballgames</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
	
	<header>
	    <div id="logo">
	        <h1>Digital Upscale Manager for Ballgames</h1>
	        <h2>Managing Ballgames With Technology</h2>
        </div>
	</header>
    
    <!-- Middle section, all centered content and form stuff goes here -->
    <section>
		<article>
			<p>Please select a customer:
                <asp:DropDownList ID="ddlCustomerList" runat="server" DataSourceID="SqlCustomerData" DataTextField="Name" DataValueField="Name" AutoPostBack="True" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlCustomerData" runat="server" ConnectionString="<%$ ConnectionStrings:CustomerConnect %>" ProviderName="<%$ ConnectionStrings:CustomerConnect.ProviderName %>" SelectCommand="SELECT [CustomerID], [Name], [Address], [State], [City], [ZipCode], [Phone], [Email] FROM [Customer]"></asp:SqlDataSource>
            </p>
            <p>
                <!-- I know that text boxes are looked down upon for data display, but if their "Read Only"
                    property is set to "True" will that justify them? It seems to me that property wouldn't
                    be there unless they also were for data display? -->
                Name:<asp:TextBox ID="txtName" runat="server" ReadOnly="True"></asp:TextBox>
                <br/>
                Address:<asp:TextBox ID="txtAddress" runat="server" ReadOnly="True"></asp:TextBox>
                <br/>
                City:<asp:TextBox ID="txtCity" runat="server" ReadOnly="True"></asp:TextBox>
                <br/>
                State:<asp:TextBox ID="txtState" runat="server" ReadOnly="True"></asp:TextBox>
                <br/>
                Zip Code:<asp:TextBox ID="txtZipCode" runat="server" ReadOnly="True"></asp:TextBox>
                <br/>
                Phone #:<asp:TextBox ID="txtPhone" runat="server" ReadOnly="True"></asp:TextBox>
                <br/>
                Email:<asp:TextBox ID="txtEmail" runat="server" ReadOnly="True"></asp:TextBox>
                <br/>
                Customer ID:<asp:TextBox ID="txtCustomerID" runat="server" ReadOnly="True"></asp:TextBox>
            </p>
            <p>
                <asp:Button ID="btnViewContacts" runat="server" Text="View Contact List" OnClick="btnViewContacts_Click" />
                <asp:Button ID="btnAddToContacts" runat="server" OnClick="btnAddToContacts_Click"/>
            </p>
            <p>
                <asp:Label ID="lblSystemMessages" runat="server" ForeColor="#00CC00"></asp:Label>
                <br/>
            </p>
            <p>
                <asp:Button ID="btnHome" runat="server" OnClick="btnHome_Click" Text="&lt;--- Back to Homepage" />
            </p>
		</article>
	</section>
    
    <!-- A copyright notice and disclaimer because why not -->
	<footer>©Copyright Justin McConnell 2015
        <br/>
        Disclaimer: Not responsible for any financial loss or bodily injury due to use of the information or code on this site.
	</footer>

    </form>
</body>
</html>

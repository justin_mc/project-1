﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI;

public partial class Default : Page
{
    private Customer _selectedCustomer;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            this.ddlCustomerList.DataBind();
        }

        this.ChangeCustomerInfo();
    }

    /// <summary>
    /// Gets the selected customer from the data source.
    /// </summary>
    /// <returns></returns>
    private Customer GetSelectedCustomer()
    {
        DataView customerTable = (DataView) this.SqlCustomerData.Select(DataSourceSelectArguments.Empty);
        DataRowView row = customerTable[this.ddlCustomerList.SelectedIndex];

        Customer currentCustomer = new Customer();
        currentCustomer.Name = row["Name"].ToString();
        currentCustomer.Address = row["Address"].ToString();
        currentCustomer.City = row["City"].ToString();
        currentCustomer.State = row["State"].ToString();
        currentCustomer.ZipCode = row["ZipCode"].ToString();
        currentCustomer.Phone = row["Phone"].ToString();
        currentCustomer.Email = row["Email"].ToString();
        currentCustomer.CustomerId = row["CustomerID"].ToString();
        return currentCustomer;
    }

    /// <summary>
    /// Changes the customer information to the ones set in the selected customer.
    /// </summary>
    private void ChangeCustomerInfo()
    {
        this._selectedCustomer = this.GetSelectedCustomer();
        this.txtName.Text = this._selectedCustomer.Name;
        this.txtAddress.Text = this._selectedCustomer.Address;
        this.txtCity.Text = this._selectedCustomer.City;
        this.txtState.Text = this._selectedCustomer.State;
        this.txtZipCode.Text = this._selectedCustomer.ZipCode;
        this.txtPhone.Text = this._selectedCustomer.Phone;
        this.txtEmail.Text = this._selectedCustomer.Email;
        this.txtCustomerID.Text = this._selectedCustomer.CustomerId;
        this.btnAddToContacts.Text = "Add " + this._selectedCustomer.Name + " To Contacts";
    }
    /// <summary>
    /// Handles the SelectedIndexChanged event of the ddlCustomerList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ChangeCustomerInfo();
        this.lblSystemMessages.Text = "";
    }
    protected void btnViewContacts_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContactList.aspx");
    }
    /// <summary>
    /// Adds the currently selected customer to the contacts list.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAddToContacts_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            String name = this._selectedCustomer.Name;
            CustomerList list = CustomerList.GetList();
            ListItem listItem = list[name];
            if (listItem == null)
            {
                list.AddItem(new ListItem(this._selectedCustomer));
                this.lblSystemMessages.Text = name + " was added to the contacts list.";
            }
            else
            {
                this.lblSystemMessages.Text = name + " was already added to the contacts list.";
            }
        }
    }
    protected void btnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}
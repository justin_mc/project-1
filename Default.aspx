﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Digital Upscale Manager for Ballgames</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
     <header>
	    <div id="logo">
	        <h1>Digital Upscale Manager for Ballgames</h1>
	        <h2>Managing Ballgames With Technology</h2>
        </div>
	</header>
    <section>
        <article>
            <p>
                
                Welcome.</p>
            <p>
                
                <asp:Button ID="btnGoCustomerList" runat="server" Text="Go to Customer List -&gt;" OnClick="btnGoCustomerList_Click" Width="260px" />
                <br/>
                <asp:Button ID="btnGoFeedback" runat="server" Text="Go to Feedback -&gt;" OnClick="btnGoFeedback_Click" Width="260px" />
                
                
            </p>
        </article>
    </section>
            
    <!-- A copyright notice and disclaimer because why not -->
	<footer>©Copyright Justin McConnell 2015
        <br/>
        Disclaimer: Not responsible for any financial loss or bodily injury due to use of the information or code on this site.
	</footer>
    </form>
</body>
</html>

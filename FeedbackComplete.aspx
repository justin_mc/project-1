﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FeedbackComplete.aspx.cs" Inherits="FeedbackComplete" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Digital Upscale Manager for Ballgames</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
	
	<header>
	    <div>
	        <h1>Digital Upscale Manager for Ballgames</h1>
	        <h2>Managing Ballgames With Technology</h2>
        </div>
	</header>
    
    <!-- Middle section, all centered content and form stuff goes here -->
    <section>
        <article>
            <p>
                <asp:Label ID="lblMessage" runat="server" ForeColor="#00CC00" Text="Thank you for the feedback! Someone will be contacting you shortly."></asp:Label>
            </p>
            <p>
                <asp:Button ID="btnReturn" runat="server" OnClick="btnReturn_Click" Text="Return to Feedback Page" />
            </p>
        </article>
    </section>
        </form>
        <!-- A copyright notice and disclaimer because why not -->
	<footer>©Copyright Justin McConnell 2015
        <br/>
        Disclaimer: Not responsible for any financial loss or bodily injury due to use of the information or code on this site.
	</footer>
</body>
</html>
